<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once __DIR__ . '/helper.php';

$input = JFactory::getApplication()->input;
$view  = $input->getString('view');

if($view == "article")
{
    require( JModuleHelper::getLayoutPath( 'mod_pray', "AllView" ) );
}
else
{
    require( JModuleHelper::getLayoutPath( 'mod_pray' ) );
}

