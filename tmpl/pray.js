(function($) {
    var ItemId = 1;

    // я помолюсь
    $('.addCountPray').live('click', function(event) {
        event.preventDefault();

        var id = $(this).closest('li').data('id'); // Получаем ID просьбы
        var target = $('li[data-id='+id+'] .countPray'); // Устанавливаем контейнер для вывода данных

        /// Проверяем соглашался ли пользователь помолиться за эту просьбу сегодня и запрещаем если да.

        if(getCookie(id.toString()) != undefined) {
            alertPray('Вы уже подтвердили намерение о молитве за эту просьбу');
            return;
            }
        /// Устанавливаем колличество отвеченных за день просьб
        setCookie(id + ' = ' + 1);

        /// Проверяем сколько раз согласились на просьбу и если больше 1 увеличиваем счётчи, меньше обновляем cookie
        var countPrays = getCookie('Prays');
        if(countPrays == undefined)
            countPrays = new Number(0);
        else{
            countPrays = parseInt(countPrays);
            if(countPrays > 100){
                $('.alertPray p').html('Нельзя соглошаться на молитву больше пяти раз в сутки');
                $('.alertPray').css('display','block');
                setTimeout(function() {
                    $('.alertPray').css('display','none')
                }, 10000);
                return;
            }
        }
        countPrays += 1;
        /// Устанавливаем колличество отвеченных за день просьб
        setCookie('Prays = ' + countPrays);

        var request = {
            'option': 'com_ajax', // Используем AJAX интерфейс
            'module': 'pray', // Название модуля без mod_
            'format': 'json', // Формат возвращаемых данных
            'idPrey': id, // ID пользователя
            'method' : 'AddCount',
            'Itemid' : ItemId
        };

        // Посылаем AJAX запрос
        $.ajax({
            type: 'POST',
            data: request,
        })
            .done(function(response) {
                if (response.success) {
                    // обновляем колличество помолившихся
                    target.html(response.data);
                }
            });
    });

    // удалить просьбу
    $('.prays .deletePray').live('click', function(event) {
        event.preventDefault();
        var target = $(this).closest('li'); // Устанавливаем контейнер для вывода данных
        var id = target.data('id'); // Получаем ID просьбы

        // Формируем параметры запроса
        var request = {
            'option': 'com_ajax', // Используем AJAX интерфейс
            'module': 'pray', // Название модуля без mod_
            'format': 'json', // Формат возвращаемых данных
            'idPrey': id, // ID пользователя
            'method' : 'Delete',
            'Itemid' : ItemId
        };

        // Посылаем AJAX запрос
        $.ajax({
            type: 'POST',
            data: request,
        })
            .done(function(response) {
                if (response.success  && response.data) {
                    // обновляем колличество помолившихся
                    target.remove();
                }
            });
    });

    // отправить просьбу
    $('.btnprey.send').on('click', function(event) {

        var text = $('textarea.prey').val(); // Получаем ID пользователя
        if(text == '') return;
        event.preventDefault();
        var target = $('ul.prays'); // Устанавливаем контейнер для вывода данных

        // Формируем параметры запроса
        var request = {
            'option': 'com_ajax', // Используем AJAX интерфейс
            'module': 'pray', // Название модуля без mod_
            'format': 'json', // Формат возвращаемых данных
            'text': text, // ID пользователя
            'method' : 'AddPrey',
            'Itemid' : ItemId
        };

        // Посылаем AJAX запрос
        $.ajax({
            type: 'POST',
            data: request,
        })
            .done(function(response) {
                if (response.success) {
                    clearArea();
                    if(response.data == true){

                        alertPray('Спасибо, ваш просьба будет опубликована после проверки модератором', 'success');
                        return;
                    }

                    var result =
                        '<li data-id="' + response.data + '"><p>' + text + '</p>' +
                            '<p class="bottom">' +
                                '<a class="addCountPray" href="#">Я за это помолюсь</a>' +
                                '<span class="deletePray">X</span>' +
                                '<span class="countPray">0</span>' +
                            '</p>' +
                        '</li>';
                    target.prepend(result);
                }
            });
    });

    // опубликовать просьбу
    $('.prays .publishPray').live('click', function(event) {
        event.preventDefault();
        var target = $(this); // Устанавливаем контейнер для вывода данных
        var id = target.closest('li').data('id'); // Получаем ID просьбы

        // Формируем параметры запроса
        var request = {
            'option': 'com_ajax', // Используем AJAX интерфейс
            'module': 'pray', // Название модуля без mod_
            'format': 'json', // Формат возвращаемых данных
            'idPrey': id, // ID пользователя
            'method' : 'PublishPrey',
            'Itemid' : ItemId
        };

        // Посылаем AJAX запрос
        $.ajax({
            type: 'POST',
            data: request,
        })
            .done(function(response) {
                if (response.success  && response.data) {
                    // обновляем колличество помолившихся
                    target.remove();
                }
            });
    });


    // добавить просьбу
    $('.add-pray').on('click', function(event){
        event.preventDefault();
        $('.prayText').css('display','block');
    })

    // отменить просьбу
    $('.btnprey.cancel').on('click', function(event){
        event.preventDefault();
        clearArea();
    })

    // спрятать и очистить поле ввода просьбы
    function clearArea(){
        $('.prayText').css('display','none');
        $('textarea.prey').val('')
    }

    function alertPray(text, status) {
        if(status == 'success'){
            $('.alertPray').addClass('alertSuccessPray');
            $('.alertPray').removeClass('alertErrorPray')
            $('.alertPray h5').html('Спасибо');
        } else {
            $('.alertPray').addClass('alertErrorPray');
            $('.alertPray').removeClass('alertSuccessPray')
            $('.alertPray h5').html('Ошибка');
        }

        $('.alertPray p').html(text);

        $('.alertPray').css('display','block');
        setTimeout(function() {
            $('.alertPray').css('display','none')
        }, 10000);
    }
})(jQuery);

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(cookie) {
    var date = new Date ;
    date.setDate(date.getDate() + 1)
    date.setSeconds(0);
    date.setHours(0);
    date.setMinutes(0);
    var updatedCookie = cookie;
    updatedCookie += '; path=/; expires=' + date.toUTCString();
    document.cookie = updatedCookie;
}

