<? defined( '_JEXEC' ) or die( 'Restricted access' );
require_once __DIR__ . '/../helper.php';

$isAdministrator = ModPrayHelper::IsAdministrator();

$db = JFactory::getDBO();

/*Pagination*/
$sql = 'SELECT COUNT(*) FROM `#__pray`';
if(!$isAdministrator){
    $sql .= ' WHERE Published = 1';
}
$db->setQuery($sql);
$db->query();
$countPrays = $db->loadRow();
$pages = ceil($countPrays[0] / 20);
$currentPage = JFactory::getApplication()->input->getInt('page');

if($currentPage == "" || $currentPage <= 0 || $currentPage > $pages)
    $currentPage = 1;
$currentPage--;
/*End Pagination*/

$sql = 'SELECT * FROM `#__pray`';
if(!$isAdministrator){
    $sql .= ' WHERE Published = 1';
}
$sql .= ' ORDER BY `Id` DESC LIMIT ' . ($currentPage * 20) . ', ' . 20;
$db->setQuery($sql);echo $sql;
$data_rows_assoc_list = $db->loadAssocList();
?>
<link rel="stylesheet" href="modules/mod_pray/tmpl/Pray.css" type="text/css">
<section class="ModPrays">
    <header><h4><?php echo JText::_('PRAY_PLEASE_PRAY'); ?></h4></header>
    <div class="bottom">
        <a href="" class="add-pray"><?php echo JText::_('PRAY_ADD_REQUEST'); ?></a>
    </div>
    <br>
    <div class="alertPray" style="display:none">
        <button type="button" class="closePray" data-dismiss="alert">×</button>
        <h5 class="alert-heading"></h5>
        <p></p>
    </div>
    <div class="prayText">
        <form action="#" id="prayFrom">
            <textarea class="prey" name="prayer" maxlength="360" placeholder="<?php echo JText::_('PRAY_WRITE_HERE_YOUR_REQUEST'); ?>" required='required' style="width: 100%" rows="5"></textarea>
            <div class="pray-req-ctrl">
                <span class="">
                    <button class="btnprey cancel" type="button" value="Отенить"><?php echo JText::_('PRAY_CANCEL'); ?></button>
                    <button class="btnprey send" data-all="true" name="PleasePray" value="Отправить"><?php echo JText::_('PRAY_SEND'); ?></button>
                </span>
            </div>
            <br>
        </form>
    </div>

    <ul class="prays">
        <? $count = count($data_rows_assoc_list);
        for($i = 0; $i < $count; $i++)
        {?>

            <li data-id="<?=$data_rows_assoc_list[$i]['Id'];?>">
                <p><?=$data_rows_assoc_list[$i]['Molitva'];?></p>
                <p class="bottom">
                    <a class="addCountPray" href="#"><?php echo JText::_('PRAY_I_PRAY_FOR_IT'); ?></a>
                    <?if ($isAdministrator) {?>

                        <? // Если не опубликовано
                        if($data_rows_assoc_list[$i]['Published'] == '0'){?>
                            <span class="publishPray">V</span>
                        <?}?>
                        <span class="deletePray">X</span>
                    <?}?>
                    <span class="countPray"><?=$data_rows_assoc_list[$i]['quantity'];?></span>
                </p>
            </li>

        <?}?>
    </ul>
    <p class="pagination">
        <?for($i = 1; $i <= $pages;$i++ ){?>
        <a <?if($i == $currentPage + 1)echo 'class="activePage"';?> href="Prays?page=<?=$i?>"><?=$i?></a>
        <?}?>
    </p>
</section>

<script src="modules/mod_pray/tmpl/pray.js"></script>