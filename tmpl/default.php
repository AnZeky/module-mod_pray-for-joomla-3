<? defined( '_JEXEC' ) or die( 'Restricted access' );

$db= JFactory::getDBO();
$sql = 'SELECT * FROM `#__pray` WHERE Published=1 ORDER BY `Id` DESC LIMIT 0, 4';
$db->setQuery($sql);
$data_rows_assoc_list = $db->loadAssocList();
 
?>
<link rel="stylesheet" href="modules/mod_pray/tmpl/Pray.css" type="text/css">
<section class="ModPrays">
    <h3 class="header"><?php echo JText::_('PRAY_PLEASE_PRAY'); ?></h3>
    <div class="alertPray alertErrorPray" style="display:none">
        <button type="button" class="closePray" data-dismiss="alert">×</button>
        <h5 class="alert-heading"><?php echo JText::_('PRAY_ERROR'); ?></h5>
        <p></p>
    </div>
    <ul class="prays">
        <? $count = count($data_rows_assoc_list);
        for($i = 0; $i < $count; $i++)
        {?>

            <li data-id="<?=$data_rows_assoc_list[$i]['Id'];?>">
                <p><?=$data_rows_assoc_list[$i]['Molitva'];?></p>
                <p class="bottom">
                    <a data-id="<?=$data_rows_assoc_list[$i]['Id'];?>" id="prey-id" class="addCountPray" href="#"><?php echo JText::_('PRAY_I_PRAY_FOR_IT'); ?></a>
                    <span class="countPray"><?=$data_rows_assoc_list[$i]['quantity'];?></span>
                </p>
            </li>

        <?}?>
    </ul>
    <div class="bottom">
        <a href="" class="add-pray"><?php echo JText::_('PRAY_ADD_REQUEST'); ?></a>
        <a href="./Prays"><?php echo JText::_('PRAY_ALL_REQUESTS'); ?></a>
    </div>
    <div class="prayText">
        <form action="#" id="prayFrom">
            <textarea class="prey" name="prayer" maxlength="360" placeholder="<?php echo JText::_('PRAY_WRITE_HERE_YOUR_REQUEST'); ?>" required='required' style="width: 100%" rows="5"></textarea>
            <div class="pray-req-ctrl">
                    <span class="">
                        <button class="btnprey cancel" type="button"><?php echo JText::_('PRAY_CANCEL'); ?></button>
                        <button class="btnprey send" name="PleasePray"><?php echo JText::_('PRAY_SEND'); ?></button>
                    </span>
            </div>
        </form>
    </div>
</section>

<script src="modules/mod_pray/tmpl/pray.js"></script>
