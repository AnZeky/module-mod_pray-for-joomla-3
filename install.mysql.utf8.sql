DROP TABLE IF EXISTS `#__pray`;


CREATE TABLE IF NOT EXISTS `#__pray` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `Molitva` text NOT NULL,
  `Published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;