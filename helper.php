<?php defined( '_JEXEC' ) or die( 'Restricted access' );

class ModPrayHelper
{
    public static function AddCountAjax()
    {
        // получаем id просьбы
        $input = JFactory::getApplication()->input;
        $id = $input->getInt('idPrey');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $fields = array($db->quoteName('quantity') . '= `quantity` + 1');
        $conditions = array($db->quoteName('id') . ' = ' . $id);
        $query->update($db->quoteName('#__pray'))
            ->set($fields)
            ->where($conditions);
        // увеличиваем на одну
        $db->setQuery($query)
            ->execute();

        // запрашиваем количество откликнувшихся
        $db = JFactory::getDBO();
        $sql = 'SELECT quantity FROM `#__pray` WHERE Id = ' . $id;
        $db->setQuery($sql);
        $data_rows_assoc_list = $db->loadAssocList();
        // ������� ����������� ���������
        return $data_rows_assoc_list[0]['quantity']; //�� ������ ���-������ ������� � �����
    }

    public static function AddPreyAjax()
    {
        $isAdmin = ModPrayHelper::IsAdministrator();

        // получаем id просьбы
        $input = JFactory::getApplication()->input;
        $text = $input->getString('text', '');
        $idPrey = $input->getInt('Itemid');
        $pray = new stdClass();
        $pray->Molitva = $text;
        $pray->MailHash = md5($text);
        // Просьбы админстраторов не требуют проверки
        if ($isAdmin)
            $pray->Published = 1;
        else
            $pray->Published = 0;

        $result = JFactory::getDbo()->insertObject('#__pray', $pray);
        $insertId = JFactory::getDbo()->insertid();

        if (!$result)
            return false;
        if (!$isAdmin) {
            $buttonResponse = '<br><p class="response">
<a class="publishPray"
href="' . JURI::base() . '?option=com_ajax&module=pray&format=text&email=1&idPrey=' . $insertId . '&method=PublishMail&Itemid=' . $idPrey . '&MailHash=' . $pray->MailHash . '"
style="
    background: lightgreen;
    display: inline-block;
    padding: 5px 10px;
    color: #fff;
    font-size: 13px;
    position: relative;
    left: 9px;">V</a>
<a class="deletePray"
href="' . JURI::base() . '?option=com_ajax&module=pray&format=text&email=1&idPrey=' . $insertId . '&method=DeleteMail&Itemid=' . $idPrey . '&MailHash=' . $pray->MailHash . '"
style="
    display: inline-block;
    padding: 5px 10px;
    background: #b80000;
    color: #fff;
    font-size: 13px;
    position: relative;
    left: 9px;">X</a></p>';
            $body = 'Подтвердите просьбу: ' . $text . $buttonResponse;
            JFactory::getMailer()->sendMail("pray@katolik-gomel.by", "Модуль молитв", "mitskevich@mail.ru", "Новая просьба", $body, true);
            return true;
        }
        if (JFactory::getApplication()->input->getString('view', '') != 'article')
            return true;
        return $insertId;
    }

    public static function DeleteAjax()
    {
        // проверка прав администратора
        if (!ModPrayHelper::IsAdministrator()) {
            return false;
        }

        if (self::DeletePray()) {
            return true;
        } else {
            return false;
        }
    }

    public static function DeleteMailAjax()
    {
        if (self::IsAdministratorHash())
            return "Требуется авторизация";

        if (self::DeletePray())
            return "Запись удалена";
        else
            return "Запись уже удалена";

    }

    private static function DeletePray()
    {
        // получаем id просьбы
        $input = JFactory::getApplication()->input;
        $id = $input->getInt('idPrey');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array(
            $db->quoteName('id') . ' = ' . $id
        );
        $query->delete($db->quoteName('#__pray'));
        $query->where($conditions);
        $success = $db->setQuery($query)
            ->execute();

        if ($success) {
            return true;
        } else {
            return false;
        }
    }

    public static function PublishPreyAjax()
    {
        // проверка прав администратора
        if (!ModPrayHelper::IsAdministrator()) {
            return false;
        }

        if (self::DeletePray()) {
            return true;
        } else {
            return false;
        }
    }

    public static function PublishMailAjax()
    {
        if (self::IsAdministratorHash())
            return "Требуется авторизация";

        if (self::PublishPray())
            return "Запись опубликованна";
        else
            return "Запись не опубликованна";

    }

    private static function PublishPray()
    {
        // получаем id просьбы
        $input = JFactory::getApplication()->input;
        $id = $input->getInt('idPrey');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $fields = array($db->quoteName('Published') . '= 1');
        $conditions = array($db->quoteName('id') . ' = ' . $id);
        $query->update($db->quoteName('#__pray'))
            ->set($fields)
            ->where($conditions);

        $success = $db->setQuery($query)
            ->execute();

        if ($success) {
            return true;
        } else {
            return false;
        }
    }


    public static function IsAdministrator()
    {
        $userGroups = JFactory::getUser()->groups;
        // Если Администратор
        if (array_key_exists('8', $userGroups))
            return true;
        // Если редактор
        if (array_key_exists('4', $userGroups))
            return true;
        // Не админитстатор
        return false;
    }

    public static function IsAdministratorHash()
    {
        $input = JFactory::getApplication()->input;
        $id = $input->getInt('idPrey');
        $MailHash = $input->getString('MailHash', '');
        $db= JFactory::getDBO();
        $sql = 'SELECT `MailHash` FROM `#__pray` WHERE Id = ' . $id;
        $db->setQuery($sql);
        $data_rows_assoc_list = $db->loadAssoc();
        if ($data_rows_assoc_list['MailHash'] != $MailHash)
            // правильный hash
            return true;
        else
            return false;
    }
}